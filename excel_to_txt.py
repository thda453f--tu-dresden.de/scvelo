#!/usr/bin/env python
# coding: utf-8

# In[90]:


import pandas as pd

# Path to the Excel file
excel_file = "20230530_sequencingInformation_franziskab.xlsx"

# Read the Excel file
df = pd.read_excel(excel_file)
df.head()


# In[91]:


df=df.loc[:,["LibraryID","Name"]]
df.drop_duplicates(inplace=True)


# In[92]:


df.reset_index(drop=True,inplace=True)


# In[93]:


df


# In[94]:


df["patient"] = df["Name"].str.split("_").str[0]


# In[95]:


df["cancer"] = df["Name"].apply(lambda x: "gastric" if x.startswith("OO") else "colon" if x.startswith("DD") else "")
df


# In[96]:


df2=pd.DataFrame({"LibraryID":["DD191_2","DD191_4","DD191_5","DD218","DD282","DD341_4"],
                 "Name":["DD191_2","DD191_4","DD191_5","DD218","DD282","DD341_4"],
                 "cancer":["gastric"]*6,
                  "patient":["DD191_2","DD191_4","DD191_5","DD218","DD282","DD341_4"]
                 })
df2


# In[97]:


df_all=pd.concat([df2, df], ignore_index=True)


# In[98]:


df_all


# In[99]:


df_gastric=df_all[df_all["cancer"]=="gastric"]
df_gastric


# In[100]:


df_gastric["treated"]=df_gastric["Name"].apply(lambda x: "vivo" if "cT" in x else "vitro" if "_treated" in x else "no")
df_gastric.drop("cancer",axis=1,inplace=True)
df_gastric


# In[102]:


df_gastric["sample"]=df_gastric["patient"].str.cat(df_gastric["treated"],sep="-")
df_gastric


# In[109]:


df_gastric["ArrayTaskID"]=range(1,df_gastric.shape[0]+1)


# In[110]:


df_gastric


# In[111]:


# Convert the Excel file to a tab-separated text file
output_file = "gastric_meta.txt"
df_gastric.to_csv(output_file, sep="\t", index=False)


# In[115]:


df_colon=df_all[df_all["cancer"]=="colon"]
df_colon["medium"]=df_colon["Name"].str.split("_").str[1]
df_colon


# In[116]:


df_colon["ArrayTaskID"]=range(1,df_colon.shape[0]+1)
df_colon


# In[ ]:


df_colon.to_csv("colon_meta.txt",sep="\t",index=False)

